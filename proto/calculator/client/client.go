package main

import (
	"context"
	"fmt"
	"gRPC_course/proto/calculator/calculatorpb"
	"io"
	"log"

	"google.golang.org/grpc"
)

func main() {
	fmt.Println("Hello from calculator client")

	// change WithInsecure for production for SSL
	conn, err := grpc.Dial(":8000", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Could not connect to port 8000 : %v", err)
	}
	defer conn.Close()

	c := calculatorpb.NewCalculatorServiceClient(conn)
	// fmt.Sprintf("Created client : %f", c)
	// doUnary(c)
	doServerStreaming(c)
}

func doServerStreaming(c calculatorpb.CalculatorServiceClient) {
	fmt.Println("Starting to do a Server Streaming RPC ...")
	req := &calculatorpb.PrimeNumberDecompositionRequest{
		Number: 12390392840,
	}

	resStream, err := c.PrimeNumberDecomposition(context.Background(), req)
	if err != nil {
		log.Fatalf("Error while calling Sum RPC : %v", err)
	}

	for {
		res, err := resStream.Recv()
		if err == io.EOF {
			// We have received the end of the stream
			break
		}
		if err != nil {
			log.Printf("Error with reading server stream : %v\n", err)
		}
		log.Printf("Response from GreetServerStreaming : %v\n", res.GetPrimeFactor())
	}

}

func doUnary(c calculatorpb.CalculatorServiceClient) {
	fmt.Println("Starting to do a Unary RPC ...")
	req := &calculatorpb.SumRequest{
		FirstNumber:  5,
		SecondNumber: 40,
	}

	res, err := c.Sum(context.Background(), req)
	if err != nil {
		log.Fatalf("Error while calling Sum RPC : %v", err)
	}
	log.Printf("Response from Calculator : %v", res.SumResult)

}
