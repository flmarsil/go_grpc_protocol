package main

import (
	"context"
	"fmt"
	"gRPC_course/proto/greet/greetpb"
	"io"
	"log"
	"net"
	"strconv"
	"time"

	"google.golang.org/grpc"
)

type server struct{}

func (*server) Greet(ctx context.Context, req *greetpb.GreetRequest) (*greetpb.GreetResponse, error) {
	fmt.Printf("Greet function was invoked with : %v\n", req)
	firstname := req.GetGreeting().GetFirstName()
	result := "Hello " + firstname
	response := greetpb.GreetResponse{
		Result: result,
	}
	return &response, nil
}

func (*server) GreetServerStreaming(req *greetpb.GreetServerStreamingRequest, stream greetpb.GreetService_GreetServerStreamingServer) error {
	fmt.Printf("GreetServerStreaming function was invoked with: %v\n", req)
	firstName := req.GetGreeting().GetFirstName()
	for i := 0; i < 10; i++ {
		result := "Hello " + firstName + " number " + strconv.Itoa(i)
		response := greetpb.GreetServerStreamingResponse{
			Result: result,
		}
		stream.Send(&response)
		time.Sleep(1000 * time.Millisecond)
	}
	return nil
}

func (*server) GreetClientStreaming(stream greetpb.GreetService_GreetClientStreamingServer) error {
	fmt.Printf("GreetClientStreaming function was invoked with a streaming request\n")

	result := ""

	for {
		req, err := stream.Recv()
		if err == io.EOF {
			// We have finished to read client stream
			return stream.SendAndClose(&greetpb.GreetClientStreamingResponse{
				Result: result,
			})
		}
		if err != nil {
			log.Fatalf("Error while reading client stream : %v", err)
		}
		firstName := req.GetGreeting().GetFirstName()
		result += "Hello " + firstName + " ! "
	}
}

func main() {
	fmt.Println("Hello World!")

	lis, err := net.Listen("tcp", ":9000")
	if err != nil {
		log.Fatalf("Failed to listen : %v", err)
	}

	s := grpc.NewServer()

	greetpb.RegisterGreetServiceServer(s, &server{})
	if err := s.Serve(lis); err != nil {
		log.Fatalf("Failed to serve : %v", err)
	}
}
