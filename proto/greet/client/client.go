package main

import (
	"context"
	"fmt"
	"gRPC_course/proto/greet/greetpb"
	"io"
	"log"
	"time"

	"google.golang.org/grpc"
)

func main() {
	fmt.Println("Hello from client")

	// change WithInsecure for production for SSL
	conn, err := grpc.Dial(":9000", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Could not connect to port 9000 : %v", err)
	}
	defer conn.Close()

	c := greetpb.NewGreetServiceClient(conn)
	// fmt.Sprintf("Created client : %f", c)

	// doUnary(c)
	// doServerStreaming(c)
	doClientStreaming(c)
}

func doClientStreaming(c greetpb.GreetServiceClient) {
	fmt.Println("Starting to do a Client Streaming RPC ...")

	requests := []*greetpb.GreetClientStreamingRequest{
		&greetpb.GreetClientStreamingRequest{
			Greeting: &greetpb.Greeting{
				FirstName: "Florian",
			},
		},
		&greetpb.GreetClientStreamingRequest{
			Greeting: &greetpb.Greeting{
				FirstName: "Stephane",
			},
		},
		&greetpb.GreetClientStreamingRequest{
			Greeting: &greetpb.Greeting{
				FirstName: "John",
			},
		},
		&greetpb.GreetClientStreamingRequest{
			Greeting: &greetpb.Greeting{
				FirstName: "Pierre",
			},
		},
		&greetpb.GreetClientStreamingRequest{
			Greeting: &greetpb.Greeting{
				FirstName: "Paul",
			},
		},
	}

	stream, err := c.GreetClientStreaming(context.Background())
	if err != nil {
		log.Fatalf("Error while calling GreetClientStreaming : %v", err)
	}

	// Iterate over requests slice and send each message individually
	for _, req := range requests {
		fmt.Printf("Sending request : %v\n", req)
		stream.Send(req)
		time.Sleep(1000 * time.Millisecond)
	}

	response, err := stream.CloseAndRecv()
	if err != nil {
		log.Fatalf("Error while receiving response from GreetClientStreaming : %v", err)
	}

	fmt.Printf("GreetClientStreaming response : %v\n", response)
}

func doServerStreaming(c greetpb.GreetServiceClient) {
	fmt.Println("Starting to do a Server Streaming RPC ...")

	req := &greetpb.GreetServerStreamingRequest{
		Greeting: &greetpb.Greeting{
			FirstName: "Florian",
			LastName:  "Marsili",
		},
	}
	resStream, err := c.GreetServerStreaming(context.Background(), req)
	if err != nil {
		log.Fatalf("Error while calling GreetServerStreaming RPC : %v", err)
	}

	for {
		msg, err := resStream.Recv()
		if err == io.EOF {
			// We have received the end of the stream
			break
		}
		if err != nil {
			log.Printf("Error with reading server stream : %v", err)
		}
		log.Printf("Response from GreetServerStreaming: %v", msg.GetResult())
	}
}

func doUnary(c greetpb.GreetServiceClient) {
	fmt.Println("Starting to do a Unary RPC ...")
	req := &greetpb.GreetRequest{
		Greeting: &greetpb.Greeting{
			FirstName: "Florian",
			LastName:  "Marsili",
		},
	}

	res, err := c.Greet(context.Background(), req)
	if err != nil {
		log.Fatalf("Error while calling Greet RPC : %v", err)
	}
	log.Printf("Response from Greet : %v", res.Result)
}
